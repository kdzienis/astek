import requests
import json


def test_client_get_200():
    r = requests.get('http://127.0.0.1:5000/client/')
    assert r.status_code == 200


def test_client_get():
    r = requests.get('http://127.0.0.1:5000/client/')
    assert isinstance(r.json(), list)


def test_client_get_200_id():
    r = requests.get('http://127.0.0.1:5000/client/3')
    assert r.status_code == 200

def test_client_get__id():
    r = requests.get('http://127.0.0.1:5000/client/3')
    assert r.json()['id'] == 3


def test_client_create():
    params = {'name': 'Adam', 'ip-address': '127.0.0.1'}
    r = requests.post('http://127.0.0.1:5000/client/', params=params)
    assert r.json()['result'] == 'success'


def test_client_update():
    params = {'name': 'Adam', 'ip-address': '9.9.9.9'}
    r = requests.put('http://127.0.0.1:5000/client/3', params=params)
    assert r.json()['result'] == 'success'


# def test_client_delete():
#     r = requests.delete('http://127.0.0.1:5000/client/2')
#     assert r.json()['result'] == 'success'




def test_dataset_get_200():
    r = requests.get('http://127.0.0.1:5000/dataset/')
    assert r.status_code == 200


def test_dataset_get():
    r = requests.get('http://127.0.0.1:5000/dataset/')
    assert isinstance(r.json(), list)


def test_dataset_get_200_id():
    r = requests.get('http://127.0.0.1:5000/dataset/3')
    assert r.status_code == 200

def test_dataset_get_id():
    r = requests.get('http://127.0.0.1:5000/dataset/3')
    assert r.json()['id'] == 3


def test_dataset_create():
    params = {'filename': 'cat.jpg', 'id_client': 3}
    r = requests.post('http://127.0.0.1:5000/dataset/', params=params)
    assert r.json()['result'] == 'success'


def test_dataset_update():
    params = {'filename': 'kitty.jpg', 'id_client': 3}
    r = requests.put('http://127.0.0.1:5000/dataset/3', params=params)
    assert r.json()['result'] == 'success'


# def test_dataset_delete():
#     r = requests.delete('http://127.0.0.1:5000/dataset/2')
#     assert r.json()['result'] == 'success'

