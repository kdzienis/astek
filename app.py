from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from flask import Flask
from flask import request

from flask_restful import Resource, Api

from db_init import Base, Client, Dataset, ClientSchema, DatasetSchema

app = Flask(__name__)
api = Api(app)

engine = create_engine('sqlite:///data.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


class ClientApi(Resource):
    def get(self):
        clients = session.query(Client).all()
        schema = ClientSchema(many=True)

        return schema.dump(clients)

    def post(self):
        name = request.args['name']
        ip_address = request.args['ip-address']
        client = Client(name=name, ip_address=ip_address)
        session.add(client)
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}


class ClientListApi(Resource):
    def get(self, client_id):
        client = session.query(Client).filter(Client.id == client_id).one()
        schema = ClientSchema()
        return schema.dump(client)

    def delete(self, client_id):
        client = session.query(Client).filter(Client.id == client_id).one()
        session.delete(client)
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}

    def put(self, client_id):
        name = request.args['name']
        ip_address = request.args['ip-address']
        client = session.query(Client).filter(Client.id == client_id).one()
        client.name = name
        client.ip_address = ip_address
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}


class DatasetApi(Resource):
    def get(self):
        datasets = session.query(Dataset).all()
        schema = DatasetSchema(many=True)
        return schema.dump(datasets)

    def post(self):
        filename = request.args['filename']
        client_id = request.args['id_client']
        dataset = Dataset(filename=filename, client_id=client_id)
        session.add(dataset)
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}


class DatasetListApi(Resource):
    def get(self, dataset_id):
        dataset = session.query(Dataset).filter(Dataset.id == dataset_id).first()
        schema = DatasetSchema()
        return schema.dump(dataset)

    def delete(self, dataset_id):
        dataset = session.query(Dataset).filter(Dataset.id == dataset_id).first()
        session.delete(dataset)
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}

    def put(self, dataset_id):
        filename = request.args['filename']
        client_id = request.args['id_client']
        dataset = session.query(Dataset).filter(Dataset.id == dataset_id).one()
        dataset.filename = filename
        dataset.client_id = client_id
        try:
            session.commit()
            return {'result': 'success'}
        except:
            session.rollback()
            session.flush()
            return {'result': ' failure'}


api.add_resource(ClientApi, '/client/')
api.add_resource(DatasetApi, '/dataset/')
api.add_resource(ClientListApi, '/client/<int:client_id>')
api.add_resource(DatasetListApi, '/dataset/<int:dataset_id>')
